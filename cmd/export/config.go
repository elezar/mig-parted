/*
 * Copyright (c) 2021, NVIDIA CORPORATION.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package export

import (
	"fmt"

	"github.com/NVIDIA/mig-parted/api/spec/v1"
	"github.com/NVIDIA/mig-parted/cmd/util"
	"github.com/NVIDIA/mig-parted/pkg/mig/mode"
	"github.com/NVIDIA/mig-parted/pkg/nvpci"
	"github.com/NVIDIA/mig-parted/pkg/types"
)

func ExportMigConfigs(f *Flags) (*v1.Spec, error) {
	nvidiaModuleLoaded, err := util.IsNvidiaModuleLoaded()
	if err != nil {
		return nil, fmt.Errorf("error checking if nvidia module loaded: %v", err)
	}

	if !nvidiaModuleLoaded {
		return nil, fmt.Errorf("nvidia module must be loaded in order to export MIG config state")
	}

	nvpci := nvpci.New()
	gpus, err := nvpci.GetGPUs()
	if err != nil {
		return nil, fmt.Errorf("error enumerating GPUs: %v", err)
	}

	manager := util.NewCombinedNvmlMigManager()

	configSpecs := make([]v1.MigConfigSpec, len(gpus))
	for i, gpu := range gpus {
		deviceID := types.NewDeviceID(gpu.Device, gpu.Vendor)
		deviceFilter := deviceID.String()

		enabled := false
		capable, err := manager.IsMigCapable(i)
		if err != nil {
			return nil, fmt.Errorf("error checking MIG capable: %v", err)
		}
		if capable {
			m, err := manager.GetMigMode(i)
			if err != nil {
				return nil, fmt.Errorf("error checking MIG capable: %v", err)
			}
			enabled = (m == mode.Enabled)
		}

		migDevices := types.MigConfig{}
		if enabled {
			migDevices, err = manager.GetMigConfig(i)
			if err != nil {
				return nil, fmt.Errorf("error getting MIGConfig: %v", err)
			}
		}

		configSpecs[i] = v1.MigConfigSpec{
			DeviceFilter: deviceFilter,
			Devices:      []int{i},
			MigEnabled:   enabled,
			MigDevices:   migDevices,
		}
	}

	spec := v1.Spec{
		Version: v1.Version,
		MigConfigs: map[string][]v1.MigConfigSpec{
			f.ConfigLabel: mergeMigConfigSpecs(configSpecs),
		},
	}

	return &spec, nil
}

func mergeMigConfigSpecs(specs []v1.MigConfigSpec) []v1.MigConfigSpec {
	dfCount := make(map[string]int)
	for _, s := range specs {
		dfCount[s.DeviceFilter]++
	}

	var merged []v1.MigConfigSpec
OUTER:
	for _, s := range specs {
		if len(merged) == 0 {
			merged = append(merged, s)
			continue
		}
		for i, m := range merged {
			if s.DeviceFilter != m.DeviceFilter {
				continue
			}
			if s.MigEnabled != m.MigEnabled {
				continue
			}
			if !s.MigDevices.Equals(m.MigDevices) {
				continue
			}
			merged[i].Devices = append(m.Devices.([]int), s.Devices.([]int)...)
			if len(merged[i].Devices.([]int)) == dfCount[merged[i].DeviceFilter] {
				merged[i].Devices = "all"
			}
			continue OUTER
		}
		merged = append(merged, s)
	}
	return merged
}

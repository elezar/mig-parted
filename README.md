# MIG ***Part***iton ***Ed***itor for NVIDIA GPUs

### Build `nvidia-mig-parted`
```
go build ./cmd/nvidia-mig-parted
...
```

### Apply a config to switch all GPUs into MIG mode
```
./nvidia-mig-parted apply --mode-change-only -f examples/config.yaml -c all-enabled
...
```

### Apply a config to stripe `1g.5gb` devices across all GPUs
```
./nvidia-mig-parted apply -f examples/config.yaml -c all-1g.5gb
./nvidia-mig-parted export -o yaml
version: v1
mig-configs:
  current:
  - device-filter: "0x20B010DE"
    devices: all
    mig-enabled: true
    mig-devices:
      1g.5gb: 7
```

### Apply a config to stripe `2g.10gb` devices across all GPUs
```
./nvidia-mig-parted apply -f examples/config.yaml -c all-2g.10gb
./nvidia-mig-parted export -o yaml
version: v1
mig-configs:
  current:
  - device-filter: "0x20B010DE"
    devices: all
    mig-enabled: true
    mig-devices:
      2g.10gb: 3
```

### Apply a config to switch MIG mode on all GPUs according to a custom policy
```
./nvidia-mig-parted apply --mode-change-only -f examples/config.yaml -c custom-config-1
...
```

### Apply the same config to stripe MIG devices across the GPUs as specified
```
./nvidia-mig-parted apply -f examples/config.yaml -c custom-config-1
./nvidia-mig-parted export -o yaml
version: v1
mig-configs:
  current:
  - device-filter: "0x20B010DE"
    devices: [0, 1, 2, 3]
    mig-enabled: false
    mig-devices: {}
  - device-filter: "0x20B010DE"
    devices: [4]
    mig-enabled: true
    mig-devices:
      1g.5gb: 7
  - device-filter: "0x20B010DE"
    devices: [5]
    mig-enabled: true
    mig-devices:
      2g.10gb: 3
  - device-filter: "0x20B010DE"
    devices: [6]
    mig-enabled: true
    mig-devices:
      3g.20gb: 2
  - device-filter: "0x20B010DE"
    devices: [7]
    mig-enabled: true
    mig-devices:
      1g.5gb: 2
      2g.10gb: 1
      3g.20gb: 1
```
